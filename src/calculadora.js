/*!
 * Calculator2000
 * Author: Giuliano Stedile <giu.stedile@gmail.com>
 * Licensed under the MIT license
 */

  var defaults = {
    token      : 'asdasdasdas', // Required Token.
    secret     : '', // Required Secret.
    lang       : 'pt', // Set plugin lang (en, pt) Defaults = en
    modal      : false,
    urlRequest : 'http://teste.com.br', // Required URL to HTTP request
  };

  function Calc2000(elem, options) {

    this.elem = elem;
    this.options = options;

    this.settings = $.extend({}, this.options, defaults);

  }

  Calc2000.prototype.multply = function(n1, n2) {
      return n1 * n2;
  };

  Calc2000.prototype.divides = function(n1, n2){
    return (typeof(n1) == "number" && typeof(n2) == "number") ? (n1 / n2) : null;
  };
