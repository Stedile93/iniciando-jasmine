describe('Intanciando a classe', function(){

  it('o elemento (primeiro parametro da classe), nao pode ser undefined', function(){

    var calc = new Calc2000('#myform');

    expect(calc.elem).not.toBeUndefined();

  });

  it('options (segundo parametro da classe), pode ser undefined', function(){

    var calc = new Calc2000('#myform');

    expect(calc.options).toBeUndefined();

  });

});


describe('Multiplicação', function(){

  var calc = new Calc2000();

  it('multiplicando 2 por 5 deve retornar 10', function(){
    expect(calc.multply(2, 5)).toEqual(10);
  });

  it('multiplicando 10 por 5 deve retornar 50', function(){
    expect(calc.multply(10, 5)).toEqual(50);
  });

});


describe('Divisão', function(){

  var calc = new Calc2000();

  it('dividindo 5 por 2 deve retornar 2,5', function(){
    expect(calc.divides(5, 2)).toEqual(2.5);
  });

  it('dividindo 10 por 2 deve retornar 5', function(){
    expect(calc.divides(10, 2)).toEqual(5);
  });

  it('dividindo "string" por 2 deve retornar null', function(){
    expect(calc.divides('string', 2)).toEqual(null);
  });

  it('dividindo 10 por "string" deve retornar null', function(){
    expect(calc.divides(10, 'string')).toEqual(null);
  });

});
